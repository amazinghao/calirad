#!/usr/bin/env python
import rospy
from track_msgs.msg import Track3Array
from track_msgs.msg import Track3
from apriltags2_ros.msg import AprilTagDetectionArray

pub = rospy.Publisher('track3_array', Track3Array, queue_size=10) 

def callback(detection_msg):
    global pub
    # rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)    
    track_array = Track3Array()
    track_array.header = detection_msg.header
    if (len(detection_msg.detections) == 0):
        return
    for i in range(0,len(detection_msg.detections)):
        track = Track3()
        track.pos.x = detection_msg.detections[i].pose.pose.pose.position.x
        track.pos.y = detection_msg.detections[i].pose.pose.pose.position.y
        track.pos.z = detection_msg.detections[i].pose.pose.pose.position.z
        track.id = i
        track.status = 1
        track_array.tracks.append(track)
    pub.publish(track_array)
    
def listener():

    rospy.Subscriber("tag_detections", AprilTagDetectionArray, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':

    rospy.init_node('april_detections_to_track3_array', anonymous=True)
    listener()
