/**
 *  This file is a part of calirad.
 *
 *  Copyright (C) 2018 Juraj Persic, University of Zagreb Faculty of Electrical
 Engineering and Computing

 *  calirad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lidar_utils/polygon_tracker.h"
#include <ros/ros.h>
#include <string>

int main(int argc, char *argv[]) {

  ros::init(argc, argv, "polygon_tracking_node");

  ros::NodeHandle nh(""), nh_param("~");

  calirad::PolygonTracker polygon_tracker(nh, nh_param, "pointcloud_topic");
  ros::spin();
  ros::waitForShutdown();
}