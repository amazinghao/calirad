#include "lidar_utils/polygon_tracker.h"

using namespace calirad;

PolygonTracker::PolygonTracker(ros::NodeHandle nh, ros::NodeHandle nh_params,
                               std::string pointcloud_topic)
{
  nh_ = nh;
  nh_params_ = nh_params;
  sub_ = nh_.subscribe(pointcloud_topic, 1, &PolygonTracker::pointCloudCallback,
                       this);

  cloud_pub_ = nh_.advertise<sensor_msgs::PointCloud2>("polygon/points", 10);
  track3_pub_ = nh_.advertise<track_msgs::Track3Array>("polygon/track3_array", 1000);

  tracker_active_ = false;
  last_update_time_ = -1.0;

  LoadPolygonModel();

  ros::param::param<double>("polygon_max_speed", polygon_max_speed_, double(2.0));
  ros::param::param<double>("timeout", timeout_, double(0.5));
  ros::param::param<double>("max_distance_workspace", max_distance_workspace_, double(3.0));
  ros::param::param<double>("max_distance_update", max_distance_update_, double(1.0));
  ros::param::param<int>("lidar_beams", lidar_beams_, int(32));
  ros::param::param<double>("lidar_min_elevation", lidar_min_elevation_, double(-31.48 * M_PI / 180.0));
  ros::param::param<double>("lidar_max_elevation", lidar_max_elevation_, double(10.67 * M_PI / 180.0));
  ros::param::param<double>("cut_angle", cut_angle_, double(3.1416));
  ros::param::param<double>("sampling_time", sampling_time_, double(0.1));
  ros::param::param<bool>("azimuth_timestamp_compensation", azimuth_timestamp_compensation_, bool(true));

  lidar_vertical_resolution_ =
      (lidar_max_elevation_ - lidar_min_elevation_) / (lidar_beams_ - 1);

  lidar_to_triangle_transform_.setIdentity();
  pcl::console::setVerbosityLevel(pcl::console::L_ALWAYS);

  if (cut_angle_ < 0.0)
  {
    cut_angle_ = cut_angle_ + 2 * M_PI;
  }

  time_scale_ = sampling_time_ / (2 * M_PI);
};

// Loads triangle points from config file and creates vertices
void PolygonTracker::LoadPolygonModel()
{
  std::vector<double> points_x, points_y, origin;
  ros::param::param<std::vector<double>>("points_x", points_x);
  ros::param::param<std::vector<double>>("points_y", points_x);

  if (points_x.size() == 0)
  {
    points_x = {-0.19, 0.19, 0.0};
  }
  if (points_y.size() == 0)
  {
    points_y = {-0.10, -0.10, 0.40};
  }
  origin = {0.0, 0.0};

  polygon_model_.origin.x = origin[0];
  polygon_model_.origin.y = origin[1];
  polygon_model_.origin.z = 0.0;
  polygon_model_.vertices_.resize(0);
  polygon_model_.edges_.resize(0);
  PolygonVertex vertex;
  PolygonEdge edge;
  for (size_t i = 0; i < 3; i++)
  {
    vertex.point.x = points_x[i];
    vertex.point.y = points_y[i];
    vertex.point.z = 0.0;
    vertex.edge1 = (i + 2) % 3;
    vertex.edge2 = i;
    Eigen::Vector3f point1(points_x[(i + 2) % 3], points_y[(i + 2) % 3], 0.0);
    Eigen::Vector3f point2(points_x[(i + 1) % 3], points_y[(i + 1) % 3], 0.0);
    Eigen::Vector3f vertex_point(points_x[i], points_y[i], 0.0);
    vertex.distance1 = (point1 - vertex_point).norm();
    vertex.distance2 = (point2 - vertex_point).norm();
    Eigen::Vector3f diff1 = (vertex_point - point1).normalized();
    Eigen::Vector3f diff2 = (vertex_point - point2).normalized();
    vertex.angle = acos(diff1.dot(diff2));
    edge.polygon_edge_id = i;
    polygon_model_.vertices_.push_back(vertex);
  }
};

// Main callback function. It either tries to find initializing triangle
// detection (more conservative) or it updated the tracker using less
// conservative detections.
void PolygonTracker::pointCloudCallback(
    const sensor_msgs::PointCloud2ConstPtr &msg)
{
  double current_time; // time_difference;
  current_time = msg->header.stamp.sec + msg->header.stamp.nsec * 1e-9;
  header_ = msg->header;
  if (current_time == 0.0)
  {
    std::cerr << "Header timestamp empty.\n";
  }
  time_difference_ = current_time - last_update_time_;
  bool tracker_updated;

  input_cloud_.reset(new pcl::PointCloud<pcl::PointXYZI>);
  triangle_publish_.reset(new pcl::PointCloud<pcl::PointXYZI>);
  triangle_publish_candidate_.reset(new pcl::PointCloud<pcl::PointXYZI>);
  subsampled_cloud_.reset(new pcl::PointCloud<pcl::PointXYZI>);
  pcl::fromROSMsg(*msg, *input_cloud_);

  if (time_difference_ > timeout_)
  {
    tracker_updated = initializeDetection();
    if (tracker_updated)
    {
      std::cout << "Tracker initialized.\n";
    }
  }
  else
  {
    tracker_updated = updateDetection();
  }

  if (tracker_updated)
  {
    last_update_time_ = current_time;
  }
};

bool PolygonTracker::initializeDetection()
{
  auto start = std::chrono::system_clock::now();
  cropBox(0, 0, 0, max_distance_workspace_);
  removeGroundPlane();
  filterVoxelGrid();
  bool triangle_found;
  triangle_found = normalClustering();
  auto end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end - start;
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);

  return triangle_found;
};

bool PolygonTracker::updateDetection()
{
  auto start = std::chrono::system_clock::now();
  cropBox(translation_[0], translation_[1], translation_[2],
          max_distance_update_);

  removeGroundPlane();
  bool triangle_found;
  triangle_found = findPolygonPlane();

  if (triangle_found)
  {
    publishTrackArray();
    sensor_msgs::PointCloud2 cloud_msg;
    pcl::toROSMsg(*triangle_publish_, cloud_msg);
    cloud_msg.header = header_;
    cloud_pub_.publish(cloud_msg);
  }

  auto end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end - start;
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);

  return triangle_found;
};

bool PolygonTracker::findPolygonPlane()
{
  pcl::SACSegmentation<pcl::PointXYZI> seg;
  pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
  pcl::PointCloud<pcl::PointXYZI>::Ptr triangle_candidate(
      new pcl::PointCloud<pcl::PointXYZI>);
  pcl::PointCloud<pcl::PointXYZI>::Ptr triangle_candidate_final(
      new pcl::PointCloud<pcl::PointXYZI>);
  seg.setOptimizeCoefficients(true);
  seg.setModelType(pcl::SACMODEL_PERPENDICULAR_PLANE);
  seg.setMethodType(pcl::SAC_RANSAC);
  seg.setDistanceThreshold(0.05);
  Eigen::Matrix3d R;
  R = angle_axis_.toRotationMatrix();
  Eigen::Vector3f plane_normal;
  plane_normal << R(0, 2), R(1, 2), R(2, 2);
  seg.setAxis(plane_normal);
  seg.setEpsAngle(0.2); // 5°
  seg.setInputCloud(input_cloud_);

  double triangle_fit;
  double min_triangle_fit = 1e10;
  Eigen::AngleAxis<double> angle_axis, angle_axis_candidate;
  Eigen::Vector3d translation, translation_candidate;
  bool triangle_found = false;
  int triangle_candidates_found = 0;

  pcl::search::KdTree<pcl::PointXYZI>::Ptr tree(
      new pcl::search::KdTree<pcl::PointXYZI>);
  tree->setInputCloud(input_cloud_);

  std::vector<pcl::PointIndices> cluster_indices;
  pcl::EuclideanClusterExtraction<pcl::PointXYZI> ec;
  ec.setClusterTolerance(0.10);
  ec.setMinClusterSize(20);
  ec.setMaxClusterSize(25000);
  ec.setSearchMethod(tree);
  ec.setInputCloud(input_cloud_);
  ec.extract(cluster_indices);

  /* Loop goes through obtained euclidean clusters. If they comply with a plane model, 
  they are considered as triangle candidates. If multiple triangles are found, use none.
  */
  for (size_t i = 0; i < cluster_indices.size(); i++)
  {
    pcl::ExtractIndices<pcl::PointXYZI> extract;
    extract.setInputCloud(input_cloud_);
    pcl::PointIndices::Ptr indices_ptr(new pcl::PointIndices);
    *indices_ptr = cluster_indices[i];
    extract.setIndices(indices_ptr);
    extract.setNegative(false);
    extract.filter(*triangle_candidate);

    while (triangle_candidate->size() > 20)
    {
      seg.setInputCloud(triangle_candidate);
      seg.segment(*inliers, *coefficients);

      if (inliers->indices.size() == 0)
      {
        break;
      }
      triangle_fit = fitTriangle(*inliers, triangle_candidate, angle_axis,
                                 translation, 0.8);

      if (triangle_fit >= 0.0 && triangle_fit < 0.02)
      {
        triangle_candidates_found += 1;
        if (triangle_fit < min_triangle_fit)
        {
          *triangle_publish_ = *triangle_publish_candidate_;
          angle_axis_candidate = angle_axis;
          translation_candidate = translation;
          min_triangle_fit = triangle_fit;
          triangle_found = true;
        }
      }

      extract.setInputCloud(triangle_candidate);
      extract.setIndices(inliers);
      extract.setNegative(true);
      extract.filter(*triangle_candidate);
    }
  }

  // Perform outlier rejections scheme
  if (triangle_candidates_found > 1)
  {
    // std::cout << "Multiple triangle cadidates found...\n";
    return false;
  }
  double triangle_speed =
      (translation_ - translation_candidate).norm() / time_difference_;
  if (triangle_speed > polygon_max_speed_)
  {
    // std::cout << triangle_speed
              // << "  Triangle moved too fast, possible outlier....\n";
    return false;
  }
  if (triangle_found)
  {
    angle_axis_ = angle_axis_candidate;
    translation_ = translation_candidate;
    setTF2Transform(angle_axis_, translation_);
    broadcastEstimatedTransform();
    return true;
  }
  else
  {
    std::cout << "Triangle not found\n";
    return false;
  }
}

void PolygonTracker::filterVoxelGrid()
{
  pcl::VoxelGrid<pcl::PointXYZI> vg;
  pcl::PointCloud<pcl::PointXYZI>::Ptr cloud_filtered(
      new pcl::PointCloud<pcl::PointXYZI>);
  vg.setInputCloud(input_cloud_);
  vg.setLeafSize(0.03f, 0.03f, 0.03f);
  vg.filter(*subsampled_cloud_);
}
void PolygonTracker::removeGroundPlane()
{
  pcl::ModelCoefficients::Ptr coeffs_gp(new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers_gp(new pcl::PointIndices);
  pcl::SACSegmentation<pcl::PointXYZI> plane_segmentation;
  plane_segmentation.setModelType(pcl::SACMODEL_PERPENDICULAR_PLANE);
  plane_segmentation.setMethodType(pcl::SAC_RANSAC);
  plane_segmentation.setDistanceThreshold(0.05);
  Eigen::Vector3f groundNormalGP;
  groundNormalGP << 0.0, 0.0, 1.0;
  plane_segmentation.setAxis(groundNormalGP);
  plane_segmentation.setEpsAngle(0.2); // 5°
  plane_segmentation.setOptimizeCoefficients(true);
  plane_segmentation.setInputCloud(input_cloud_);
  plane_segmentation.segment(*inliers_gp, *coeffs_gp);
  double inlier_percentage =
      inliers_gp->indices.size() / double(input_cloud_->size());
  if (inlier_percentage < 0.3)
  {
    return;
  }

  pcl::ExtractIndices<pcl::PointXYZI> extract_gp;

  extract_gp.setInputCloud(input_cloud_);
  extract_gp.setIndices(inliers_gp);
  extract_gp.setNegative(true);
  extract_gp.filter(*input_cloud_);
}

void PolygonTracker::cropBox(double x, double y, double z, double distance_x,
                             double distance_y, double distance_z)
{
  pcl::CropBox<pcl::PointXYZI> crop;
  Eigen::Vector4f box_min_point;
  box_min_point << (x - distance_x), (y - distance_y), (z - distance_z), 1;
  Eigen::Vector4f box_max_point;
  box_max_point << (x + distance_x), (y + distance_y), (z + distance_z), 1;
  Eigen::Vector3f box_translation;
  box_translation << x, y, z;
  Eigen::Vector3f box_rotation;
  box_rotation << 0.0, 0.0, atan2(y, x);

  crop.setMin(box_min_point);
  crop.setMax(box_max_point);
  crop.setTranslation(box_translation);
  crop.setRotation(box_rotation);
  crop.setInputCloud(input_cloud_);
  crop.filter(*input_cloud_);
}

void PolygonTracker::cropBox(double x, double y, double z, double distance)
{
  pcl::CropBox<pcl::PointXYZI> crop;
  Eigen::Vector4f box_min_point;
  box_min_point << (-distance), (-distance), (-distance), 1;
  Eigen::Vector4f box_max_point;
  box_max_point << (+distance), (+distance), (+distance), 1;
  Eigen::Vector3f box_translation;
  box_translation << x, y, z;
  Eigen::Vector3f box_rotation;
  box_rotation << 0.0, 0.0, atan2(y, x);

  crop.setMin(box_min_point);
  crop.setMax(box_max_point);
  crop.setTranslation(box_translation);
  crop.setRotation(box_rotation);
  crop.setInputCloud(input_cloud_);
  crop.filter(*input_cloud_);
}

bool PolygonTracker::normalClustering()
{
  pcl::search::Search<pcl::PointXYZI>::Ptr tree(
      new pcl::search::KdTree<pcl::PointXYZI>);
  pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
  pcl::NormalEstimation<pcl::PointXYZI, pcl::Normal> normal_estimator;
  normal_estimator.setSearchMethod(tree);
  normal_estimator.setInputCloud(subsampled_cloud_);
  normal_estimator.setKSearch(50);
  normal_estimator.compute(*normals);

  pcl::RegionGrowing<pcl::PointXYZI, pcl::Normal> reg;
  reg.setMinClusterSize(30);
  reg.setMaxClusterSize(300);
  reg.setSearchMethod(tree);
  reg.setNumberOfNeighbours(40);
  reg.setInputCloud(subsampled_cloud_);
  reg.setInputNormals(normals);
  reg.setSmoothnessThreshold(10.0 / 180.0 * M_PI);
  reg.setCurvatureThreshold(0.2);

  std::vector<pcl::PointIndices> clusters;
  reg.extract(clusters);

  int counter = 0;
  double triangle_fit;
  double min_triangle_fit = 1e10;
  Eigen::AngleAxis<double> angle_axis;
  Eigen::Vector3d translation;
  bool triangle_found = false;
  while (counter < clusters.size())
  {
    triangle_fit = fitTriangle(clusters[counter], subsampled_cloud_, angle_axis,
                               translation, 0.9);
    if (triangle_fit >= 0.0 && triangle_fit < min_triangle_fit &&
        triangle_fit < 0.01)
    {
      angle_axis_ = angle_axis;
      translation_ = translation;
      min_triangle_fit = triangle_fit;
      triangle_found = true;
    }
    counter++;
  }
  if (triangle_found)
  {
    setTF2Transform(angle_axis_, translation_);
    broadcastEstimatedTransform();
    return true;
  }
  else
  {
    return false;
  }
}

double PolygonTracker::fitTriangle(pcl::PointIndices &indices,
                                   pcl::PointCloud<pcl::PointXYZI>::Ptr cloud,
                                   Eigen::AngleAxis<double> &angle_axis,
                                   Eigen::Vector3d &translation,
                                   double required_inlier_percentage)
{
  pcl::ExtractIndices<pcl::PointXYZI> extract;
  pcl::PointIndices::Ptr indices_ptr(new pcl::PointIndices);
  *indices_ptr = indices;
  pcl::PointCloud<pcl::PointXYZI>::Ptr triangle_candidate(
      new pcl::PointCloud<pcl::PointXYZI>);
  extract.setInputCloud(cloud);
  extract.setIndices(
      boost::shared_ptr<pcl::PointIndices>(new pcl::PointIndices(indices)));
  extract.setNegative(false);
  extract.filter(*triangle_candidate);

  pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
  // Create the segmentation object
  pcl::SACSegmentation<pcl::PointXYZI> seg;
  // Optional
  seg.setOptimizeCoefficients(true);
  // Mandatory
  seg.setModelType(pcl::SACMODEL_PLANE);
  seg.setMethodType(pcl::SAC_RANSAC);
  seg.setDistanceThreshold(0.03);

  seg.setInputCloud(triangle_candidate);
  seg.segment(*inliers, *coefficients);
  double inliers_percentage =
      double(inliers->indices.size()) / triangle_candidate->size();
  if (inliers_percentage < required_inlier_percentage)
  {
    return -1;
  }
  extract.setInputCloud(triangle_candidate);
  extract.setIndices(inliers);
  extract.setNegative(false);
  extract.filter(*triangle_publish_candidate_);

  // Project the model inliers
  pcl::PointCloud<pcl::PointXYZI>::Ptr cloud_projected(
      new pcl::PointCloud<pcl::PointXYZI>);
  pcl::ProjectInliers<pcl::PointXYZI> proj;
  proj.setModelType(pcl::SACMODEL_PLANE);
  proj.setIndices(inliers);
  proj.setInputCloud(triangle_candidate);
  proj.setModelCoefficients(coefficients);
  proj.filter(*cloud_projected);

  pcl::PointIndices::Ptr min_indices(new pcl::PointIndices);
  pcl::PointIndices::Ptr max_indices(new pcl::PointIndices);
  pcl::PointXYZI centroid;

  findBoundryPoints(cloud_projected, min_indices, max_indices, 3, centroid);

  // Extract the inliers
  pcl::PointCloud<pcl::PointXYZI>::Ptr min_points(
      new pcl::PointCloud<pcl::PointXYZI>);
  pcl::PointCloud<pcl::PointXYZI>::Ptr max_points(
      new pcl::PointCloud<pcl::PointXYZI>);
  extract.setInputCloud(cloud_projected);
  extract.setIndices(min_indices);
  extract.setNegative(false);
  extract.filter(*min_points);
  extract.setIndices(max_indices);
  extract.setNegative(false);
  extract.filter(*max_points);

  // extract all possible lines
  std::vector<PolygonEdge> detected_edges;

  double edge_points_percentage;
  edge_points_percentage = (min_points->size() + max_points->size()) /
                           double(cloud_projected->size());

  extractLines(min_points, detected_edges);
  extractLines(max_points, detected_edges);

  std::vector<PolygonVertex> detected_vertices;

  if (!DetectVertices(detected_edges, detected_vertices))
  {
    return -1;
  }

  double triangle_avg_cost;
  triangle_avg_cost = CoarseTriangleLocalization(
      detected_edges, detected_vertices, centroid, angle_axis, translation);
  if (edge_points_percentage < 0.2)
  {

    return triangle_avg_cost;
  }
  else
  {
    return -1;
  }
}

void PolygonTracker::findBoundryPoints(
    pcl::PointCloud<pcl::PointXYZI>::Ptr cloud, pcl::PointIndices::Ptr min,
    pcl::PointIndices::Ptr max, int min_slice_points,
    pcl::PointXYZI &centroid)
{
  min->indices.clear();
  max->indices.clear();
  std::vector<int> min_indices(lidar_beams_, -1);
  std::vector<int> max_indices(lidar_beams_, -1);
  std::vector<double> min_azimuth(lidar_beams_, -1);
  std::vector<double> max_azimuth(lidar_beams_, -1);
  std::vector<int> beam_count(lidar_beams_, 0);
  bool avoid_singularity = false;
  centroid.x = 0.0;
  centroid.y = 0.0;
  centroid.z = 0.0;
  for (size_t i = 0; i < cloud->size(); i++)
  {
    double azimuth = atan2(cloud->points[i].y, cloud->points[i].x);
    if (avoid_singularity && azimuth < 0.0)
    {
      azimuth = azimuth + 2 * M_PI;
    }
    centroid.x += cloud->points[i].x;
    centroid.y += cloud->points[i].y;
    centroid.z += cloud->points[i].z;
    double range = sqrt(cloud->points[i].x * cloud->points[i].x +
                        cloud->points[i].y * cloud->points[i].y +
                        cloud->points[i].z * cloud->points[i].z);
    double elevation = asin(cloud->points[i].z / range);
    int beam =
        round((elevation - lidar_min_elevation_) / lidar_vertical_resolution_);

    beam_count.at(beam) += 1;
    if (min_indices[beam] == -1)
    {
      min_indices[beam] = i;
      max_indices[beam] = i;
      if (abs(azimuth) > M_PI / 2.0)
      {
        avoid_singularity = true;
      }
      if (avoid_singularity && azimuth < 0.0)
      {
        azimuth = azimuth + 2 * M_PI;
      }
      min_azimuth[beam] = azimuth;
      max_azimuth[beam] = azimuth;
    }
    else if (azimuth > max_azimuth[beam])
    {
      max_indices[beam] = i;
      max_azimuth[beam] = azimuth;
    }
    else if (azimuth < min_azimuth[beam])
    {
      min_indices[beam] = i;
      min_azimuth[beam] = azimuth;
    }
  }
  centroid.x /= cloud->size();
  centroid.y /= cloud->size();
  centroid.z /= cloud->size();
  for (size_t i = 0; i < lidar_beams_; i++)
  {
    if (min_indices[i] != -1 && beam_count[i] >= min_slice_points)
    {
      min->indices.push_back(min_indices[i]);
      max->indices.push_back(max_indices[i]);
    }
  }
}

void PolygonTracker::extractLines(pcl::PointCloud<pcl::PointXYZI>::Ptr cloud,
                                  std::vector<PolygonEdge> &edges)
{
  pcl::PointIndices::Ptr edge_indices(new pcl::PointIndices);
  pcl::ExtractIndices<pcl::PointXYZI> extractLine;
  pcl::SACSegmentation<pcl::PointXYZI> seg;

  seg.setOptimizeCoefficients(true);
  seg.setModelType(pcl::SACMODEL_LINE);
  seg.setMethodType(pcl::SAC_RANSAC);
  seg.setDistanceThreshold(0.03);
  seg.setInputCloud(cloud);
  bool line_found = true;
  while (cloud->size() > 2 && line_found)
  {
    PolygonEdge edge_candidate;
    seg.segment(*edge_indices, *edge_candidate.coefficients);
    if (edge_indices->indices.size() > 2)
    {
      extractLine.setInputCloud(cloud);
      extractLine.setIndices(edge_indices);
      extractLine.setNegative(false);
      extractLine.filter(*edge_candidate.cloud);
      extractLine.setNegative(true);
      extractLine.filter(*cloud);
      edges.push_back(edge_candidate);
    }
    else
    {
      line_found = false;
    }
  }
}

double PolygonTracker::CoarseTriangleLocalization(
    std::vector<PolygonEdge> &edges, std::vector<PolygonVertex> &vertices,
    pcl::PointXYZI &centroid, Eigen::AngleAxis<double> &angle_axis,
    Eigen::Vector3d &translation)
{
  double average_reprojection_error = 1e10;
  for (size_t i = 0; i < vertices.size(); i++)
  {
    for (size_t j = 0; j < polygon_model_.vertices_.size(); j++)
    {
      if (abs(vertices[i].angle - polygon_model_.vertices_[j].angle) <
          (5 * M_PI / 180.0))
      {
        // determine edges' direction sign: 4 combinations, choose one with
        // smallest angle between vertice and triangle centroid
        Eigen::Vector3d vertex(vertices[i].point.x, vertices[i].point.y,
                               vertices[i].point.z);
        Eigen::Vector3d center(centroid.x, centroid.y, centroid.z);
        Eigen::Vector3d edge1(edges[vertices[i].edge1].coefficients->values[3],
                              edges[vertices[i].edge1].coefficients->values[4],
                              edges[vertices[i].edge1].coefficients->values[5]);
        Eigen::Vector3d edge2(edges[vertices[i].edge2].coefficients->values[3],
                              edges[vertices[i].edge2].coefficients->values[4],
                              edges[vertices[i].edge2].coefficients->values[5]);

        Eigen::Vector3d vc = (center - vertex);
        // center too far away from the vertex.
        if (vc.norm() > 1)
          break;
        // determine edge direction from the vertex
        vc.normalize();
        double angle_vc_mid_edge, angle_vc_mid_edge_candidate;
        Eigen::Vector3d edge1_candidate, edge2_candidate;
        Eigen::Vector3d mid_edge = (edge1 + edge2).normalized();
        angle_vc_mid_edge = acos(vc.dot(mid_edge));
        edge1_candidate = edge1;
        edge2_candidate = edge2;

        Eigen::Vector3d mid_edge_candidate = (-edge1 + edge2).normalized();
        angle_vc_mid_edge_candidate = acos(vc.dot(mid_edge_candidate));
        if (angle_vc_mid_edge_candidate < angle_vc_mid_edge)
        {
          edge1_candidate = -edge1;
          angle_vc_mid_edge = angle_vc_mid_edge_candidate;
          mid_edge = mid_edge_candidate;
        }
        mid_edge_candidate = (edge1 - edge2).normalized();
        angle_vc_mid_edge_candidate = acos(vc.dot(mid_edge_candidate));
        if (angle_vc_mid_edge_candidate < angle_vc_mid_edge)
        {
          edge2_candidate = -edge2;
          angle_vc_mid_edge = angle_vc_mid_edge_candidate;
          mid_edge = mid_edge_candidate;
        }
        mid_edge_candidate = (-edge1 - edge2).normalized();
        angle_vc_mid_edge_candidate = acos(vc.dot(mid_edge_candidate));
        if (angle_vc_mid_edge_candidate < angle_vc_mid_edge)
        {
          edge1_candidate = -edge1;
          edge2_candidate = -edge2;
          angle_vc_mid_edge = angle_vc_mid_edge_candidate;
          mid_edge = mid_edge_candidate;
        }
        std::array<Eigen::Vector3d, 3> model_points;
        std::array<Eigen::Vector3d, 3> projected_points;
        Eigen::Vector3d point;
        point << polygon_model_.vertices_[j].point.x,
            polygon_model_.vertices_[j].point.y,
            polygon_model_.vertices_[j].point.z;
        model_points[0] = point;
        point << polygon_model_.vertices_[(j + 2) % 3].point.x,
            polygon_model_.vertices_[(j + 2) % 3].point.y,
            polygon_model_.vertices_[(j + 2) % 3].point.z;
        model_points[1] = point;
        point << polygon_model_.vertices_[(j + 1) % 3].point.x,
            polygon_model_.vertices_[(j + 1) % 3].point.y,
            polygon_model_.vertices_[(j + 1) % 3].point.z;
        model_points[2] = point;
        point << vertices[i].point.x, vertices[i].point.y, vertices[i].point.z;
        projected_points[0] = point;

        bool opposite;
        double current_reprojection_error_edge1,
            current_reprojection_error_edge2;
        // Solution 1
        Eigen::Vector3d point1, point2;
        PointOnArc(mid_edge, edge1_candidate, vertex,
                   polygon_model_.vertices_[j].angle / 2,
                   polygon_model_.vertices_[j].distance1, point1);
        PointOnArc(mid_edge, edge1_candidate, vertex,
                   -polygon_model_.vertices_[j].angle / 2,
                   polygon_model_.vertices_[j].distance2, point2);
        projected_points[1] = point1;
        projected_points[2] = point2;
        DistanceToLineSegment(vertex, point1, edges[vertices[i].edge1],
                              current_reprojection_error_edge1);
        DistanceToLineSegment(vertex, point2, edges[vertices[i].edge2],
                              current_reprojection_error_edge2);

        SVDPointRegistration(model_points, projected_points, &angle_axis,
                             &translation, &opposite);

        if (opposite)
        {
          // Solution 2
          Eigen::Vector3d point3, point4;
          PointOnArc(mid_edge, edge1_candidate, vertex,
                     -polygon_model_.vertices_[j].angle / 2,
                     polygon_model_.vertices_[j].distance1, point3);
          PointOnArc(mid_edge, edge1_candidate, vertex,
                     polygon_model_.vertices_[j].angle / 2,
                     polygon_model_.vertices_[j].distance2, point4);
          projected_points[1] = point3;
          projected_points[2] = point4;
          DistanceToLineSegment(vertex, point1, edges[vertices[i].edge1],
                                current_reprojection_error_edge1);
          DistanceToLineSegment(vertex, point2, edges[vertices[i].edge2],
                                current_reprojection_error_edge2);
          SVDPointRegistration(model_points, projected_points, &angle_axis,
                               &translation, &opposite);
        }
        double max_error = std::max(current_reprojection_error_edge1,
                                    current_reprojection_error_edge2);
        if (max_error < average_reprojection_error)
        {
          average_reprojection_error = max_error;
        }
      }
    }
  }
  return average_reprojection_error;
}

void PolygonTracker::setTF2Transform(Eigen::AngleAxis<double> &angle_axis_eigen,
                                     Eigen::Vector3d &translation)
{
  Eigen::Vector3d angle_axis;

  double angle = angle_axis_eigen.angle();
  Eigen::Vector3d axis;
  if (angle != 0.0)
  {
    axis = angle_axis_eigen.axis();
  }
  else
  {
    axis << 0, 0, 1;
  }

  tf2::Vector3 tf2_axis(axis[0], axis[1], axis[2]);
  tf2::Quaternion tf2_quaternion(tf2_axis, angle);
  tf2::Vector3 tf2_translation(translation(0), translation(1), translation(2));

  lidar_to_triangle_transform_.setOrigin(tf2_translation);
  lidar_to_triangle_transform_.setRotation(tf2_quaternion);
}

// From http://paulbourke.net/geometry/pointlineplane/
bool PolygonTracker::LinesIntersection(pcl::ModelCoefficients::Ptr line1,
                                       pcl::ModelCoefficients::Ptr line2,
                                       PolygonVertex &vertex)
{
  typedef struct
  {
    double x, y, z;
  } XYZ;

  /*
     Calculate the line segment PaPb that is the shortest route between
     two lines P1P2 and P3P4. Calculate also the values of mua and mub where
        Pa = P1 + mua (P2 - P1)
        Pb = P3 + mub (P4 - P3)
     Return FALSE if no solution exists.
  */

  XYZ pa, pb, p1, p2, p3, p4;
  XYZ p13, p43, p21;
  double mua, mub;
  double d1343, d4321, d1321, d4343, d2121;
  double numer, denom;
  double eps = 1e-3;
  double max_dinstance = 0.1;

  p1.x = line1->values[0];
  p1.y = line1->values[1];
  p1.z = line1->values[2];
  p2.x = line1->values[0] + line1->values[3];
  p2.y = line1->values[1] + line1->values[4];
  p2.z = line1->values[2] + line1->values[5];
  p3.x = line2->values[0];
  p3.y = line2->values[1];
  p3.z = line2->values[2];
  p4.x = line2->values[0] + line2->values[3];
  p4.y = line2->values[1] + line2->values[4];
  p4.z = line2->values[2] + line2->values[5];

  p13.x = p1.x - p3.x;
  p13.y = p1.y - p3.y;
  p13.z = p1.z - p3.z;
  p43.x = p4.x - p3.x;
  p43.y = p4.y - p3.y;
  p43.z = p4.z - p3.z;
  if (abs(p43.x) < eps && abs(p43.y) < eps && abs(p43.z) < eps)
    return false;
  p21.x = p2.x - p1.x;
  p21.y = p2.y - p1.y;
  p21.z = p2.z - p1.z;
  if (abs(p21.x) < eps && abs(p21.y) < eps && abs(p21.z) < eps)
    return false;

  d1343 = p13.x * p43.x + p13.y * p43.y + p13.z * p43.z;
  d4321 = p43.x * p21.x + p43.y * p21.y + p43.z * p21.z;
  d1321 = p13.x * p21.x + p13.y * p21.y + p13.z * p21.z;
  d4343 = p43.x * p43.x + p43.y * p43.y + p43.z * p43.z;
  d2121 = p21.x * p21.x + p21.y * p21.y + p21.z * p21.z;

  denom = d2121 * d4343 - d4321 * d4321;
  if (abs(denom) < eps)
    return false;
  numer = d1343 * d4321 - d1321 * d4343;

  mua = numer / denom;
  mub = (d1343 + d4321 * (mua)) / d4343;

  pa.x = p1.x + mua * p21.x;
  pa.y = p1.y + mua * p21.y;
  pa.z = p1.z + mua * p21.z;
  pb.x = p3.x + mub * p43.x;
  pb.y = p3.y + mub * p43.y;
  pb.z = p3.z + mub * p43.z;

  vertex.point.x = (pa.x + pb.x) / 2;
  vertex.point.y = (pa.y + pb.y) / 2;
  vertex.point.z = (pa.z + pb.z) / 2;
  vertex.angle = acos(line1->values[3] * line2->values[3] +
                      line1->values[4] * line2->values[4] +
                      line1->values[5] * line2->values[5]);
  double distance =
      pow(pow(pa.x - pb.x, 2) + pow(pa.y - pb.y, 2) + pow(pa.z - pb.z, 2), 0.5);
  if (distance < max_dinstance)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool PolygonTracker::DetectVertices(std::vector<PolygonEdge> &edges,
                                    std::vector<PolygonVertex> &vertices)
{
  if (edges.size() > 3)
  {
    return false;
  }
  else if (edges.size() > 1)
  {
    for (size_t i = 0; i < (edges.size() - 1); i++)
    {
      for (size_t j = i + 1; j < edges.size(); j++)
      {
        PolygonVertex vertice_candidate;
        int slice_count =
            std::min(edges[i].cloud->size(), edges[j].cloud->size());
        if (slice_count > 3 &&
            LinesIntersection(edges[i].coefficients, edges[j].coefficients,
                              vertice_candidate))
        {
          vertice_candidate.edge1 = i;
          vertice_candidate.edge2 = j;
          vertices.push_back(vertice_candidate);
        }
      }
    }
  }
  if (vertices.size() > 0)
  {
    return true;
  }
  else
  {
    return false;
  }
}

void PolygonTracker::PointOnArc(Eigen::Vector3d &u, Eigen::Vector3d &v,
                                Eigen::Vector3d &center, double angle,
                                double radius, Eigen::Vector3d &point)
{
  double angle_vectors = acos((u.normalized()).dot(v.normalized()));
  point = center + (sin(angle_vectors - angle) * u + sin(angle) * v) * radius /
                       sin(angle_vectors);
};

void PolygonTracker::DistanceToLineSegment(Eigen::Vector3d &p1,
                                           Eigen::Vector3d &p2,
                                           PolygonEdge &edge,
                                           double &distance)
{
  Eigen::Vector3d d = (p2 - p1);
  distance = 0.0;
  for (size_t i = 0; i < edge.cloud->size(); i++)
  {
    Eigen::Vector3d q;
    q(0) = edge.cloud->points[i].x;
    q(1) = edge.cloud->points[i].y;
    q(2) = edge.cloud->points[i].z;
    Eigen::Vector3d v = (q - p1);
    double t = (v.dot(d)) / pow(d.norm(), 2);
    if (t > 0.0 && t < 1.0)
    {
      Eigen::Vector3d q_projection = p1 + t * d;
      distance += (q_projection - q).norm();
    }
    else
    {
      distance += std::min((q - p1).norm(), (q - p2).norm());
    }
  }
  distance /= double(edge.cloud->size());
  return;
};

void PolygonTracker::SVDPointRegistration(
    std::array<Eigen::Vector3d, 3> &model,
    std::array<Eigen::Vector3d, 3> &detected,
    Eigen::AngleAxis<double> *angle_axis, Eigen::Vector3d *translation,
    bool *opposite)
{
  int correspondence_number = model.size();
  Eigen::Vector3d center_model(0, 0, 0), center_detected(0, 0, 0);

  for (int i = 0; i < correspondence_number; ++i)
  {
    center_model += model[i];
    center_detected += detected[i];
  }

  center_model /= (double)correspondence_number;
  center_detected /= (double)correspondence_number;

  Eigen::MatrixXd S(3, correspondence_number), D(3, correspondence_number);
  for (int i = 0; i < 3; ++i)
  {
    for (int j = 0; j < correspondence_number; ++j)
    {
      S(i, j) = model[j](i) - center_model(i);
    }
    for (int j = 0; j < 3; ++j)
    {
      D(i, j) = detected[j](i) - center_detected(i);
    }
  }

  Eigen::MatrixXd St = S.transpose();
  Eigen::MatrixXd Dt = D.transpose();
  Eigen::Matrix3d H = S * Dt;
  Eigen::Matrix3d W, U, V;

  Eigen::JacobiSVD<Eigen::MatrixXd> svd;
  Eigen::MatrixXd H_alt(3, 3);

  for (int i = 0; i < 3; ++i)
    for (int j = 0; j < 3; ++j)
      H_alt(i, j) = H(i, j);
  svd.compute(H_alt, Eigen::ComputeThinU | Eigen::ComputeThinV);
  if (!svd.computeU() || !svd.computeV())
  {
    std::cerr << "decomposition error" << std::endl;
    return;
  }

  Eigen::Matrix3d Ut = svd.matrixU().transpose();
  Eigen::Matrix3d R = svd.matrixV() * Ut;
  if (R.determinant() < 0.0)
  {
    for (int i = 0; i < 3; ++i)
    {
      R(i, 2) = -R(i, 2);
    }
  }
  *translation = center_detected - R * center_model;

  Eigen::Vector3d z_axis = R.block(0, 2, 3, 1);
  double angle_z_radii = acos(z_axis.dot(translation->normalized()));
  if (angle_z_radii < M_PI / 2.0)
  {
    *opposite = true;
  }
  else
  {
    *opposite = false;
  }
  angle_axis->fromRotationMatrix(R);
}

void PolygonTracker::broadcastEstimatedTransform()
{
  tf2_ros::TransformListener tfListener(tf_buffer_);

  geometry_msgs::TransformStamped lidar_to_triangle_transform_stamped;

  std::string frame_name = header_.frame_id;
  tf2::transformTF2ToMsg(lidar_to_triangle_transform_,
                         lidar_to_triangle_transform_stamped, ros::Time::now(),
                         frame_name, "triangle");

  br_.sendTransform(lidar_to_triangle_transform_stamped);
}

void PolygonTracker::publishTrackArray()
{
  track_msgs::Track3Array track_array;
  track_array.tracks.resize(1);
  track_array.header = header_;

  if (azimuth_timestamp_compensation_)
  {
    double timestamp = header_.stamp.sec + 1e-9 * header_.stamp.nsec;
    double azimuth = atan2(translation_[1], translation_[0]);
    if (azimuth < 0.0)
    {
      azimuth = azimuth + 2 * M_PI;
    }
    timestamp = timestamp + time_scale_ * fmod(cut_angle_ - azimuth, 2 * M_PI);
    header_.stamp.sec = floor(timestamp);
    header_.stamp.nsec = floor((timestamp - header_.stamp.sec) * 1e9);
  }

  for (size_t i = 0; i < 1; i++)
  {
    track_array.tracks[i].pos.x = translation_[0];
    track_array.tracks[i].pos.y = translation_[1];
    track_array.tracks[i].pos.z = translation_[2];
    Eigen::Quaternion<double> q = Eigen::Quaternion<double>(angle_axis_);
    track_array.tracks[i].quat.x = q.x();
    track_array.tracks[i].quat.y = q.y();
    track_array.tracks[i].quat.z = q.z();
    track_array.tracks[i].quat.w = q.w();
    track_array.tracks[i].id = i;
    track_array.tracks[i].status = 1;
  }
  track3_pub_.publish(track_array);
}
