## PREREQUISITE
Install PCL:


```
sudo add-apt-repository ppa:v-launchpad-jochen-sprickerhof-de/pcl
sudo apt-get update
sudo apt-get install libpcl1
```

## Usage
To test the triangle tracking node first download the [bag](https://ferhr-my.sharepoint.com/:u:/g/personal/jurajp_fer_hr/EcTHfWZnWOpEnf0bKyayiwkB5kjDnbM4NfVb6lrfpXg5gQ?e=xRRkKz) and place it in "calirad/bags" folder.
Then run:

```
roslaunch lidar_utils triangle_tracking.launch
```

Several parameters can be tuned using a config file (calirad/lidar_utils/config/triangle.yaml). 
Triangle is defined as a set of three vertices in a xy plane. 

The code has been tested with Velodyne 32E lidar for which parameters are given in the config file.
We recommend setting a fixed cut_angle in the lidar driver if temporal calibration is required. 
Additionally, the code can perform azimuth timestamp compensation.