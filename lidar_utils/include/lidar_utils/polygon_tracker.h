/**
 *  This file is a part of calirad.
 *
 *  Copyright (C) 2018 Juraj Persic, University of Zagreb Faculty of Electrical
 Engineering and Computing

 *  calirad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CALIRAD_POLYGON_TRACKER_H
#define CALIRAD_POLYGON_TRACKER_H

#include "ros/ros.h"
#include "track_msgs/Track3Array.h"
#include <Eigen/Geometry>
#include <algorithm>
#include <chrono>
#include <geometry_msgs/TransformStamped.h>
#include <math.h>
#include <pcl/console/print.h>
#include <pcl/features/normal_3d.h>
#include <pcl/filters/crop_box.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/search/kdtree.h>
#include <pcl/search/search.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/segmentation/region_growing.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/surface/concave_hull.h>
#include <pcl_ros/point_cloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Header.h>
#include <tf2/LinearMath/Transform.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_ros/transform_listener.h>

namespace calirad
{

  /** \brief Class that tracks a user-defined polygon in a pointcloud.
*
PolygonTracker subscribes to a PointCloud2 topic "pointcloud_topic" and tries to
find a user defined polygon in it. If the pointcloud is found in a previous
step, a search space can is reduced to decrease computing time. In the current state, tracking of only one triangle is implemented.
*/

  struct PolygonEdge
  {
    pcl::ModelCoefficients::Ptr coefficients;
    pcl::PointCloud<pcl::PointXYZI>::Ptr cloud;
    int polygon_edge_id;
    PolygonEdge()
    {
      coefficients.reset(new pcl::ModelCoefficients);
      cloud.reset(new pcl::PointCloud<pcl::PointXYZI>);
    }
  };
  struct PolygonVertex
  {
    pcl::PointXYZI point;
    double angle;
    double distance1;
    double distance2;
    int edge1;
    int edge2;
  };

  class PolygonModel
  {
  public:
    std::vector<PolygonVertex> vertices_;
    std::vector<PolygonEdge> edges_;
    pcl::PointXYZI origin;
  };

  class PolygonTracker
  {
  public:
    PolygonTracker(ros::NodeHandle nh, ros::NodeHandle nh_params,
                   std::string pointcloud_topic);

  private:
    void pointCloudCallback(const sensor_msgs::PointCloud2ConstPtr &msg);
    bool initializeDetection();
    bool updateDetection();
    void removeGroundPlane();
    bool normalClustering();
    void cropBox(double x, double y, double z, double distance);
    void cropBox(double x, double y, double z, double distance_x,
                 double distance_y, double distance_z);
    void filterVoxelGrid();
    void findBoundryPoints(pcl::PointCloud<pcl::PointXYZI>::Ptr cloud,
                           pcl::PointIndices::Ptr left,
                           pcl::PointIndices::Ptr right, int min_slice_points,
                           pcl::PointXYZI &centroid);
    double fitTriangle(pcl::PointIndices &indices,
                       pcl::PointCloud<pcl::PointXYZI>::Ptr cloud,
                       Eigen::AngleAxis<double> &angle_axis,
                       Eigen::Vector3d &translation,
                       double required_inlier_percentage);
    void extractLines(pcl::PointCloud<pcl::PointXYZI>::Ptr cloud,
                      std::vector<PolygonEdge> &edges); // deconstructs the cloud
    bool LinesIntersection(pcl::ModelCoefficients::Ptr line1,
                           pcl::ModelCoefficients::Ptr line2,
                           PolygonVertex &vertex);
    bool DetectVertices(std::vector<PolygonEdge> &edges,
                        std::vector<PolygonVertex> &vertices);
    double CoarseTriangleLocalization(std::vector<PolygonEdge> &edges,
                                      std::vector<PolygonVertex> &vertices,
                                      pcl::PointXYZI &centroid,
                                      Eigen::AngleAxis<double> &angle_axis,
                                      Eigen::Vector3d &translation);
    void PointOnArc(Eigen::Vector3d &u, Eigen::Vector3d &v,
                    Eigen::Vector3d &center, double angle, double radius,
                    Eigen::Vector3d &point);
    void DistanceToLineSegment(Eigen::Vector3d &p1, Eigen::Vector3d &p2,
                               PolygonEdge &edge, double &distance);
    void LoadPolygonModel();
    void SVDPointRegistration(std::array<Eigen::Vector3d, 3> &model,
                              std::array<Eigen::Vector3d, 3> &detected,
                              Eigen::AngleAxis<double> *angle_axis,
                              Eigen::Vector3d *translation, bool *opposite);
    void setTF2Transform(Eigen::AngleAxis<double> &angle_axis_eigen,
                         Eigen::Vector3d &translation);
    void broadcastEstimatedTransform();
    bool findPolygonPlane();
    void publishTrackArray();
    ros::NodeHandle nh_;
    ros::NodeHandle nh_params_;
    ros::Subscriber sub_;
    ros::Publisher cloud_pub_;
    ros::Publisher track3_pub_;
    pcl::PointCloud<pcl::PointXYZI>::Ptr input_cloud_;
    pcl::PointCloud<pcl::PointXYZI>::Ptr triangle_publish_;
    pcl::PointCloud<pcl::PointXYZI>::Ptr triangle_publish_candidate_;
    pcl::PointCloud<pcl::PointXYZI>::Ptr subsampled_cloud_;
    tf2::Transform lidar_to_triangle_transform_;
    tf2_ros::TransformBroadcaster br_;
    tf2_ros::Buffer tf_buffer_;

    double timeout_; // parametrize
    bool tracker_active_;
    std_msgs::Header header_;
    double max_distance_workspace_;
    double max_distance_update_;
    int lidar_beams_;
    double lidar_min_elevation_;
    double lidar_max_elevation_;
    double lidar_vertical_resolution_;
    PolygonModel polygon_model_;
    Eigen::AngleAxis<double> angle_axis_;
    Eigen::Vector3d translation_;
    double last_update_time_;
    double time_difference_;
    double polygon_max_speed_;
    bool azimuth_timestamp_compensation_;
    double cut_angle_;
    double sampling_time_;
    double time_scale_;
  };

} // namespace calirad

// workaround; forward-declaration of functions since they are defined in buffer_core.cpp,
// but not declared in buffer_core.h
namespace tf2
{
  void transformMsgToTF2(const geometry_msgs::Transform &msg,
                         tf2::Transform &tf2);
  void transformTF2ToMsg(const tf2::Transform &tf2,
                         geometry_msgs::TransformStamped &msg, ros::Time stamp,
                         const std::string &frame_id,
                         const std::string &child_frame_id);
} // namespace tf2

#endif // CALIRAD_POLYGON_TRACKER_H